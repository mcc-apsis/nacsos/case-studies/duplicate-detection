# Experiments for NACSOS deduplication refactoring
This directory contains scripts to make informed decisions on how to implement deduplication in NACSOS.

## Abstract size
**Q:** Could we just fit all abstracts in memory?

Based on a project with ~1.3M abstracts from openalex (via avg_abstract_len.sql):

| avg | min | max | stddev | sum | count |
| :--- | :--- | :--- | :--- | :--- | :--- |
| 4169.3142768170727381 | 1 | 73198 | 7864.239572677865 | 5288587541 | 1268455 |


**A:** We might, but there would be little headroom for other things and it's on the edge already.
5.3G characters (at least one byte per char if ASCII + overhead) would at least be 5GB of RAM.

Same for titles:

| avg | min | max | stddev | sum | count |
| :--- | :--- | :--- | :--- | :--- | :--- |
| 96.0788761661507825 | 1 | 500 | 40.4518380678854565 | 124284944 | 1293572 |

## Lookup for known IDs

**Q:** Could we just fit all IDs (+ item_ids) as a lookup dictionary in memory?

**A:** Yup, totally. A dict with ~1.3M openalex IDs with corresponding item_ids uses <200MB RAM. (via id_size.py)

Note: DOI w/o https://doi.org/ prefix!
```
2024-07-29 17:31:47,923 [INFO] import: A single uuid takes 64 bytes in RAM
2024-07-29 17:31:47,923 [INFO] import:   ... cast to string, it takes 77 bytes in RAM
2024-07-29 17:31:51,541 [INFO] import: A a list of 1,000,000 uuids takes 72,448,728 bytes | 70750.71kB | 69.09MB in RAM
2024-07-29 17:31:51,541 [INFO] import: Fetching for doi
Received batch with 3250 entries.: : 235it [00:11, 19.94it/s]
2024-07-29 17:32:13,139 [INFO] import: Retrieved 2343250 id entries for doi
2024-07-29 17:32:14,567 [INFO] import: Size of IDs: 94,418,420B | 92205.49kB | 90.04MB
2024-07-29 17:32:17,025 [INFO] import: Size of item_ids: 169,237,336B | 165270.84kB | 161.40MB
2024-07-29 17:32:20,051 [INFO] import: Size of dict: len(dict)=1,104,338 / 185,971,169B | 181612.47kB | 177.36MB
2024-07-29 17:32:20,051 [INFO] import: Fetching for openalex_id
Received batch with 1056 entries.: : 258it [00:12, 20.66it/s]
2024-07-29 17:32:40,952 [INFO] import: Retrieved 2571056 id entries for openalex_id
2024-07-29 17:32:42,720 [INFO] import: Size of IDs: 92,347,125B | 90182.74kB | 88.07MB
2024-07-29 17:32:45,551 [INFO] import: Size of item_ids: 186,225,624B | 181860.96kB | 177.60MB
2024-07-29 17:32:49,323 [INFO] import: Size of dict: len(dict)=1,331,109 / 196,281,616B | 191681.27kB | 187.19MB
2024-07-29 17:32:49,323 [INFO] import: Fetching for s2_id
Received batch with 5270 entries.: : 130it [00:07, 17.01it/s]
2024-07-29 17:33:04,106 [INFO] import: Retrieved 1295270 id entries for s2_id
2024-07-29 17:33:04,330 [INFO] import: Size of IDs: 10,693,000B | 10442.38kB | 10.20MB
2024-07-29 17:33:05,838 [INFO] import: Size of item_ids: 93,590,264B | 91396.74kB | 89.25MB
2024-07-29 17:33:05,917 [INFO] import: Size of dict: len(dict)=1 / 304B | 0.30kB | 0.00MB
2024-07-29 17:33:05,918 [INFO] import: Fetching for wos_id
Received batch with 5270 entries.: : 130it [00:05, 24.12it/s]
2024-07-29 17:33:17,087 [INFO] import: Retrieved 1295270 id entries for wos_id
2024-07-29 17:33:17,283 [INFO] import: Size of IDs: 10,693,000B | 10442.38kB | 10.20MB
2024-07-29 17:33:18,687 [INFO] import: Size of item_ids: 93,590,264B | 91396.74kB | 89.25MB
2024-07-29 17:33:18,765 [INFO] import: Size of dict: len(dict)=1 / 304B | 0.30kB | 0.00MB
2024-07-29 17:33:18,765 [INFO] import: Fetching for scopus_id
Received batch with 5270 entries.: : 130it [00:05, 22.14it/s]
2024-07-29 17:33:30,271 [INFO] import: Retrieved 1295270 id entries for scopus_id
2024-07-29 17:33:30,471 [INFO] import: Size of IDs: 10,693,000B | 10442.38kB | 10.20MB
2024-07-29 17:33:31,932 [INFO] import: Size of item_ids: 93,590,264B | 91396.74kB | 89.25MB
2024-07-29 17:33:32,011 [INFO] import: Size of dict: len(dict)=1 / 304B | 0.30kB | 0.00MB
2024-07-29 17:33:32,011 [INFO] import: Fetching for pubmed_id
Received batch with 3254 entries.: : 176it [00:08, 21.00it/s]
2024-07-29 17:33:46,656 [INFO] import: Retrieved 1753254 id entries for pubmed_id
2024-07-29 17:33:47,395 [INFO] import: Size of IDs: 55,463,405B | 54163.48kB | 52.89MB
2024-07-29 17:33:49,276 [INFO] import: Size of item_ids: 127,433,368B | 124446.65kB | 121.53MB
2024-07-29 17:33:50,670 [INFO] import: Size of dict: len(dict)=495,741 / 92,747,619B | 90573.85kB | 88.45MB
2024-07-29 17:33:50,670 [INFO] import: Fetching for dimensions_id
Received batch with 5270 entries.: : 130it [00:06, 20.15it/s]
2024-07-29 17:34:04,257 [INFO] import: Retrieved 1295270 id entries for dimensions_id
2024-07-29 17:34:04,471 [INFO] import: Size of IDs: 10,693,000B | 10442.38kB | 10.20MB
2024-07-29 17:34:05,936 [INFO] import: Size of item_ids: 93,590,264B | 91396.74kB | 89.25MB
2024-07-29 17:34:06,015 [INFO] import: Size of dict: len(dict)=1 / 304B | 0.30kB | 0.00MB
```
