import uuid
import logging

from itertools import chain
from collections import deque
from sys import getsizeof, stderr

from tqdm import tqdm
from sqlalchemy import text
from nacsos_data.db import get_engine

try:
    from reprlib import repr
except ImportError:
    pass

db_engine = get_engine(conf_file='../../nacsos-core/config/remote.env')

logging.basicConfig(format='%(asctime)s [%(levelname)s] %(name)s: %(message)s', level=logging.INFO)
logger = logging.getLogger('import')
logger.setLevel(logging.DEBUG)

# pd.options.display.max_columns = None

# PROJECT_ID = '3d761435-fe4f-4829-a33e-26fc52a46613'
PROJECT_ID = '52f62f17-cefb-4152-bcca-2fcb77541e83'


# https://code.activestate.com/recipes/577504-compute-memory-footprint-of-an-object-and-its-cont/
def total_size(o, handlers={}, verbose=False):
    """ Returns the approximate memory footprint an object and all of its contents.

    Automatically finds the contents of the following builtin containers and
    their subclasses:  tuple, list, deque, dict, set and frozenset.
    To search other containers, add handlers to iterate over their contents:

        handlers = {SomeContainerClass: iter,
                    OtherContainerClass: OtherContainerClass.get_elements}

    """
    dict_handler = lambda d: chain.from_iterable(d.items())
    all_handlers = {tuple: iter,
                    list: iter,
                    deque: iter,
                    dict: dict_handler,
                    set: iter,
                    frozenset: iter,
                    }
    all_handlers.update(handlers)  # user handlers take precedence
    seen = set()  # track which object id's have already been seen
    default_size = getsizeof(0)  # estimate sizeof object without __sizeof__

    def sizeof(o):
        if id(o) in seen:  # do not double count the same object
            return 0
        seen.add(id(o))
        s = getsizeof(o, default_size)

        if verbose:
            print(s, type(o), repr(o), file=stderr)

        for typ, handler in all_handlers.items():
            if isinstance(o, typ):
                s += sum(map(sizeof, handler(o)))
                break
        return s

    return sizeof(o)


def to_entries(batches):
    it = tqdm(batches)
    for batch in it:
        it.set_description(f'Received batch with {len(batch)} entries.')
        for r in batch:
            yield r['identifier'], r['item_id']


logger.info(f'A single uuid takes {total_size(uuid.uuid4())} bytes in RAM')
logger.info(f'  ... cast to string, it takes {total_size(str(uuid.uuid4()))} bytes in RAM')
lst_size = total_size([uuid.uuid4() for _ in range(1000000)])
logger.info(f'A a list of 1,000,000 uuids takes {lst_size:,} bytes '
            f'| {lst_size / 1024:.2f}kB '
            f'| {lst_size / 1024 / 1024:.2f}MB in RAM')

with db_engine.session() as session:
    for field in 'doi', 'openalex_id', 's2_id', 'wos_id', 'scopus_id', 'pubmed_id', 'dimensions_id':
        logger.info(f'Fetching for {field}')
        rslt = (session.execute(text(f'''
        SELECT ai.{field} as identifier, item_id
        FROM academic_item ai
        WHERE ai.project_id = :project_id
        
        UNION
        
        SELECT aiv.{field} as identifier, item_id
        FROM academic_item_variant aiv
        JOIN import ON aiv.import_id = import.import_id
        WHERE import.project_id = :project_id;
        '''), {'project_id': PROJECT_ID})).mappings().partitions(10000)

        entries = list(to_entries(rslt))
        logger.info(f'Retrieved {len(entries):,} id entries for {field}')
        sz = total_size([e[0] for e in entries])
        logger.info(f'Size of IDs: {sz:,}B '
                    f'| {sz / 1024:.2f}kB '
                    f'| {sz / 1024 / 1024:.2f}MB')
        sz = total_size([e[1] for e in entries])
        logger.info(f'Size of item_ids: {sz:,}B '
                    f'| {sz / 1024:.2f}kB '
                    f'| {sz / 1024 / 1024:.2f}MB')
        sz = total_size(dict(entries))
        logger.info(f'Size of dict: len(dict)={len(dict(entries)):,} '
                    f'/ {sz:,}B '
                    f'| {sz / 1024:.2f}kB '
                    f'| {sz / 1024 / 1024:.2f}MB')
