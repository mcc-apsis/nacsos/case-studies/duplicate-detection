SELECT item.item_id, ai.title, aiv.title, ai.publication_year, aiv.publication_year, ai.doi, aiv.doi, ai.openalex_id, aiv.openalex_id, ai.wos_id, aiv.wos_id, ai.scopus_id, aiv.scopus_id, item.item_id, item.text, aiv.abstract
FROM item
JOIN academic_item ai ON item.item_id = ai.item_id
JOIN academic_item_variant aiv ON ai.item_id = aiv.item_id
WHERE item.project_id = '3d761435-fe4f-4829-a33e-26fc52a46613'
-- WHERE item.project_id = '52f62f17-cefb-4152-bcca-2fcb77541e83'
ORDER BY item.item_id
LIMIT 1000;