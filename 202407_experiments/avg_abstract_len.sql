SELECT AVG(len), min(len), max(len), stddev(len), sum(len), count(len)
FROM (SELECT char_length(text) as len
      FROM item
      WHERE item.project_id = '52f62f17-cefb-4152-bcca-2fcb77541e83'
         -- WHERE item.project_id = '3d761435-fe4f-4829-a33e-26fc52a46613'
         -- LIMIT 10000
     ) a;

-- '3d761435-fe4f-4829-a33e-26fc52a46613'
-- +---------------------+---+----+----------------+--------+
-- |avg                  |min|max |stddev          |sum     |
-- +---------------------+---+----+----------------+--------+
-- |1554.9649930907415937|231|9426|466.592664955788|10127487|
-- +---------------------+---+----+----------------+--------+

-- '52f62f17-cefb-4152-bcca-2fcb77541e83'
-- +---------------------+---+-----+-----------------+----------+-------+
-- |avg                  |min|max  |stddev           |sum       |count  |
-- +---------------------+---+-----+-----------------+----------+-------+
-- |4169.3142768170727381|1  |73198|7864.239572677865|5288587541|1268455|
-- +---------------------+---+-----+-----------------+----------+-------+


SELECT AVG(len), min(len), max(len), stddev(len), sum(len), count(len)
FROM (SELECT char_length(title) as len
      FROM academic_item
      WHERE academic_item.project_id = '52f62f17-cefb-4152-bcca-2fcb77541e83'
         -- WHERE academic_item.project_id = '3d761435-fe4f-4829-a33e-26fc52a46613'
         -- LIMIT 1000000
     ) a;

-- '3d761435-fe4f-4829-a33e-26fc52a46613'
-- +--------------------+---+---+-------------------+------+
-- |avg                 |min|max|stddev             |sum   |
-- +--------------------+---+---+-------------------+------+
-- |105.0814292286459132|15 |312|30.4453945490173561|685236|
-- +--------------------+---+---+-------------------+------+

-- '52f62f17-cefb-4152-bcca-2fcb77541e83'
-- +-------------------+---+---+-------------------+---------+-------+
-- |avg                |min|max|stddev             |sum      |count  |
-- +-------------------+---+---+-------------------+---------+-------+
-- |96.0788761661507825|1  |500|40.4518380678854565|124284944|1293572|
-- +-------------------+---+---+-------------------+---------+-------+


SELECT AVG(len), min(len), max(len), stddev(len), sum(len), count(len),
       PERCENTILE_CONT(0.2) WITHIN GROUP(ORDER BY len) as p2,
       PERCENTILE_CONT(0.5) WITHIN GROUP(ORDER BY len) as med,
       PERCENTILE_CONT(0.8) WITHIN GROUP(ORDER BY len) as p8,
       PERCENTILE_CONT(0.9) WITHIN GROUP(ORDER BY len) as p9,
       PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY len) as p95
FROM (SELECT char_length(text) as len
      FROM item
      WHERE item.project_id = '52f62f17-cefb-4152-bcca-2fcb77541e83'
         -- WHERE item.project_id = '3d761435-fe4f-4829-a33e-26fc52a46613'
          LIMIT 500000
     ) a;
-- | avg | min | max | stddev | sum | count | p2 | med | p8 | p9 | p95 |
-- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
-- | 2763.649841089858719 | 1 | 73198 | 5135.910517273764 | 1335649859 | 483292 | 1034 | 1528 | 2318 | 3810.9000000000233 | 7929 |

