# Case studies to make informed design decisions for NACSOS

## Tips on profiling
It is kind of important to keep this code as efficient as possible.
There are many things to look out for, here are some basics:
* Doing checks in a certain order can help speed things up. 
* Going for list comprehensions instead of other for..in loops is faster.
* Compiling frequently used regexes is faster than in-place regexes.

To help identify bottlenecks, install the [line-profiler](https://plugins.jetbrains.com/plugin/16536-line-profiler)
plugin for PyCharm. This is a really great help to track down the most expensive functions.

