import asyncio
from collections import defaultdict

from nacsos_data.db.crud.items.academic import read_known_ids_map, read_item_ids_for_import
from nacsos_data.util import gather_async
from line_profiler_pycharm import profile
import logging
import uuid
import time
from matplotlib import pyplot as plt
import pandas as pd
import numpy as np
from nacsos_data.util.academic.importer import _check_known_identifiers
from nacsos_data.util.text import tokenise_item
from sqlalchemy import select, func, distinct, or_, and_, text, union, union_all, literal

from nacsos_data.db import get_engine_async
from nacsos_data.db.schemas.imports import Import, m2m_import_item_table
from nacsos_data.db.schemas.items.academic import AcademicItem, AcademicItemVariant
from nacsos_data.db.schemas.items.base import Item
from nacsos_data.db.schemas import AssignmentScope, Assignment, Annotation, BotAnnotationMetaData, BotAnnotation
from nacsos_data.util.academic.duplicate import str_to_title_slug

from nacsos_data.util.academic.readers.openalex import generate_items_from_openalex
from dotenv import dotenv_values

CONFIG = 'config/local.env'
config = dotenv_values(CONFIG)
db_engine = get_engine_async(conf_file=CONFIG)

logging.basicConfig(format='%(asctime)s [%(levelname)s] %(name)s: %(message)s', level=logging.INFO)
logger = logging.getLogger('testing')
logger.setLevel(logging.DEBUG)

ID_FIELDS = ['openalex_id', 's2_id', 'scopus_id', 'wos_id', 'pubmed_id', 'dimensions_id']


def from_source():
    for itm in generate_items_from_openalex(
            # query='"school uniform" OR "school dinners"',
            query='climate change',
            openalex_endpoint=config['NACSOS_OA_SOLR'],
            def_type='lucene',
            field='title_abstract',
            op='AND',
            batch_size=10000,
            log=logger
    ):
        itm.item_id = uuid.uuid4()
        yield itm


@profile
async def main(project_id: str, import_id: str | None = None):
    with open('data/tmp.jsonl', 'w+') as fp:
        async with db_engine.session() as session:
            known_ids: dict[str, dict[str, str]]
            known_ids = {
                id_field: await read_known_ids_map(session=session, project_id=project_id, field=id_field)
                for id_field in ID_FIELDS
            }

            # Fetch item_ids already in the import (in case the import failed at some point or is continued mid-way)
            imported_item_ids = set()  # set(await gather_async(read_item_ids_for_import(session=session, import_id=import_id)))

            # Set of item_ids for which we need to add m2m tuples later
            m2m_buffer: set[str] = set()
            # Accumulator for our vocabulary
            token_counts: defaultdict[str, int] = defaultdict(int)
            n_unknown_items = 0

            logger.info('Checking if there are any known identifiers in the new data...')
            times_dump_ = []
            times_tok_ = []
            times_dump = []
            times_tok = []
            vocab_size = []
            vocab_size_fltr = []
            for it, item in enumerate(from_source()):  # Iterate all new items
                # Check if we know this new item via some trusted identifier (e.g. openalex_id)
                known_item_id = _check_known_identifiers(item, known_ids, imported_item_ids, logger)

                # We know this new item (via an ID) and just need to add an m2m

                # We don't know this new item, add it to our buffer file and extend vocabulary
                if known_item_id is False:
                    start = time.time()
                    for tok in tokenise_item(item, lowercase=True):
                        token_counts[tok] += 1
                    end = time.time()
                    times_tok.append(end - start)
                    vocab_size.append(len(token_counts))
                    start = time.time()
                    fp.write(item.model_dump_json() + '\n')
                    end = time.time()
                    times_dump.append(end - start)
                    n_unknown_items += 1

                    # if len(times_tok) > 5000:
                    #     print()
                    #     print(np.mean(times_tok), np.median(times_tok), np.std(times_tok))
                    #     print(np.mean(times_dump), np.median(times_dump), np.std(times_dump))
                    #     times_tok = []
                    #     times_dump = []

                    if it > 0 and (it % 10000) == 0:
                        plt.clf()
                        times_tok_.append(times_tok)
                        times_dump_.append(times_dump)
                        fig, (ax1, ax4, ax2, ax3) = plt.subplots(1, 4, figsize=(15, 4))
                        ax1.set_title(f'Vocab @ {it:,} items has {len(token_counts):,} ')
                        ax1.plot(vocab_size)

                        vocab_size_fltr.append(len([(tok, cnt) for tok, cnt in token_counts.items() if cnt > 1]))
                        ax4.set_title(f'.. filtered has {vocab_size_fltr[-1]:,} tokens')
                        ax4.bar(np.arange(len(vocab_size_fltr)), vocab_size_fltr)

                        ax2.set_title(f'Time to tokenise ({np.mean(times_tok):.4f}, '
                                      f'{np.median(times_tok):.4f}, {np.std(times_tok):.4f})')
                        ax2.boxplot(times_tok_)

                        ax3.set_title(f'Time to dump ({np.mean(times_dump):.4f}, '
                                      f'{np.median(times_dump):.4f}, {np.std(times_dump):.4f})')
                        ax3.boxplot(times_dump_)

                        fig.tight_layout()
                        fig.show()

                        times_tok = []
                        times_dump = []

                # We found a match!
                elif known_item_id is not None and type(known_item_id) is str:
                    if known_item_id not in imported_item_ids:
                        m2m_buffer.add(known_item_id)
                        imported_item_ids.add(known_item_id)


if __name__ == '__main__':
    asyncio.run(main(project_id='86a4d535-0311-41f7-a934-e4ab0a465a68'))
