# Identifying import bottlenecks and oddities
Currently, the import of ~1M items is slow.
Based on a log, [this method](https://gitlab.pik-potsdam.de/mcc-apsis/nacsos/nacsos-data/-/blob/main/src/nacsos_data/util/academic/importer.py?ref_type=heads#L62) seems to take 2.5h alone.

## Analysing `find_id_duplicates`
Potential bottlenecks are:
* Reading all known ids from the database
* Growing the m2m buffer
* Tokenising and tracking token counts
* Dumping new items to file

After checking all components of this method, there seem to be no slow tasks.

### Tokenising, buffering, and dumping
The last two are covered in `scan_vocab.py` that produces figures like this:
![100k.png](img/100k.png)
![500k.png](img/500k.png)
![600k.png](img/600k.png)

This suggests, that tokenisation does not slow down with a growing vocabulary.
With a few exceptions, the time to tokenise and update the vocabulary counts is fairly consistent.
Similar for dumping the item models to file.
It is not blazing fast, but very much acceptable.
It takes around 09:50min on a laptop to process 500k items, 98% of the time is taken by directly fetching from openalex -> network cost (~2GB written to buffer file).

Interestingly, the size of the vocabulary continuously keeps growing without signs of converging (even when filtered to >=2).
There appears to be no "save" point to stop.

### ID checking
Maybe fetching a million IDs (x number of types of IDs) takes a long time?
See `id_check.py` for testing.

Output suggests, that for a project with >1.2M items, it's not an issue.
It does depend on the number of items that actually have an ID (network cost), in this case all had an OpenAlex ID and no dimensions ID.
This suggests, that query execution is around 16s and network transmission accounts for the rest.
```
2024-08-30 18:42:39,629 [DEBUG] testing: Fetching IDs for openalex_id took 0:01:22.514011 to execute.
2024-08-30 18:43:05,760 [DEBUG] testing: Fetching IDs for s2_id took 0:00:26.130337 to execute.
2024-08-30 18:43:27,673 [DEBUG] testing: Fetching IDs for scopus_id took 0:00:21.913504 to execute.
2024-08-30 18:43:45,292 [DEBUG] testing: Fetching IDs for wos_id took 0:00:17.618275 to execute.
2024-08-30 18:44:05,568 [DEBUG] testing: Fetching IDs for pubmed_id took 0:00:20.276366 to execute.
2024-08-30 18:44:22,229 [DEBUG] testing: Fetching IDs for dimensions_id took 0:00:16.660902 to execute.
```
Dictionary construction and ID lookups are very fast constant/linear thanks to hashmaps, so this is also not an issue. 

## Transactions
Ideally, we'd like to keep an import as a single transaction.
In case something fails, we'd like the database go back to normal without any half-done imports.
In the past, there have been weird uncertainties, `trans.py` aims to clear the air on that.

Summary:
* Open a session (this opens a transaction)
* Create [sub-transactions](https://www.postgresql.org/docs/current/subxacts.html) that you want to encapsulate, e.g. for each item insert (fail for single item okay, but keep everything else and continue if possible)
  * Option 1: `transaction = await session.begin_nested()` to start and do `await transaction.rollback()` on error. Important: do not use `session.rollback()`!
  * Option 2: `async with session.begin_nested():` context manager. If anything blows up inside, the context rolls back the sub-transaction. Catch any exception outside the context!
* Things outside the sub-transaction are not affected (so you don't suddenly have to wrap everything in subs).
* Performance penalty unclear, postgres documentation states that everything <64 nested deep is fine and cached, we never really nest, so should be fine
* Querying during an unfinished transaction will give you the state it is at within the transaction! Suspicion that querying gives you the state before the transaction are not true. It will however be shielded from changes in any concurrent transaction!
