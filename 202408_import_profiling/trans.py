import asyncio
from collections import defaultdict

from nacsos_data.db.crud.items.academic import read_known_ids_map, read_item_ids_for_import
from nacsos_data.util import gather_async
from line_profiler_pycharm import profile
import logging
import uuid
import time
from matplotlib import pyplot as plt
import pandas as pd
import numpy as np
from nacsos_data.util.text import tokenise_item
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select, func, distinct, or_, and_, text, union, union_all, literal, delete

from nacsos_data.db import get_engine_async
from nacsos_data.db.schemas.imports import Import, m2m_import_item_table
from nacsos_data.db.schemas.items.academic import AcademicItem, AcademicItemVariant
from nacsos_data.db.schemas.items.base import Item
from nacsos_data.db.schemas import (
    AssignmentScope,
    Assignment,
    Annotation,
    BotAnnotationMetaData,
    BotAnnotation,
    Project
)
from nacsos_data.util.academic.duplicate import str_to_title_slug

from sqlalchemy.exc import IntegrityError
from psycopg.errors import UniqueViolation, OperationalError
from nacsos_data.util.academic.readers.openalex import generate_items_from_openalex
from dotenv import dotenv_values

CONFIG = 'config/local.env'
config = dotenv_values(CONFIG)
db_engine = get_engine_async(conf_file=CONFIG)

logging.basicConfig(format='%(asctime)s [%(levelname)s] %(name)s: %(message)s', level=logging.INFO)
logger = logging.getLogger('testing')
logger.setLevel(logging.DEBUG)


async def main():
    async with db_engine.session() as session:  # type: AsyncSession
        logger.debug(await session.scalar(select(func.count(Project.project_id))))

        p = Project(project_id=uuid.uuid4(), name='TA1', description='', type='academic')
        logger.info('add')
        session.add(p)
        logger.debug(await session.scalar(select(func.count(Project.project_id))))
        logger.info('flush')
        await session.flush()
        logger.debug(await session.scalar(select(func.count(Project.project_id))))

        p = Project(project_id=uuid.uuid4(), name='TA2', description='', type='academic')
        logger.info('add')
        session.add(p)
        logger.debug(await session.scalar(select(func.count(Project.project_id))))
        logger.info('flush')
        await session.flush()
        logger.debug(await session.scalar(select(func.count(Project.project_id))))

        logger.info('rollback')
        await session.rollback()
        logger.debug(await session.scalar(select(func.count(Project.project_id))))

        p = Project(project_id=uuid.uuid4(), name='TA3', description='', type='academic')
        logger.info('add')
        session.add(p)
        logger.debug(await session.scalar(select(func.count(Project.project_id))))
        logger.info('flush')
        await session.flush()
        logger.debug(await session.scalar(select(func.count(Project.project_id))))

        try:
            #trans = await session.begin_nested()
            async with session.begin_nested() as bla:
                pid = uuid.uuid4()
                p = Project(project_id=pid, name='TA4', description='', type='academic')
                logger.info('add')
                session.add(p)
                logger.info('flush')
                await session.flush()
                logger.debug(await session.scalar(select(func.count(Project.project_id))))

                p = Project(project_id=pid, name='TA5', description='', type='academic')
                logger.info('add')
                session.add(p)
                logger.info('flush')
                await session.flush()
                logger.debug(await session.scalar(select(func.count(Project.project_id))))
        except (UniqueViolation, IntegrityError, OperationalError) as e:
            logger.error(e)
            #await trans.rollback()
            #raise e
        logger.info('post exception')
        logger.debug(await session.scalar(select(func.count(Project.project_id))))

        logger.info('delete')
        await session.execute(delete(Project).where(Project.name.like('TA%')))
        logger.debug(await session.scalar(select(func.count(Project.project_id))))


if __name__ == '__main__':
    asyncio.run(main())


# This logs:
# 2024-08-30 16:47:18,009 [DEBUG] testing: 4
# 2024-08-30 16:47:18,009 [INFO] testing: add
# 2024-08-30 16:47:18,010 [DEBUG] testing: 4
# 2024-08-30 16:47:18,010 [INFO] testing: flush
# 2024-08-30 16:47:18,013 [DEBUG] testing: 5
# 2024-08-30 16:47:18,014 [INFO] testing: add
# 2024-08-30 16:47:18,014 [DEBUG] testing: 5
# 2024-08-30 16:47:18,014 [INFO] testing: flush
# 2024-08-30 16:47:18,016 [DEBUG] testing: 6
# 2024-08-30 16:47:18,016 [INFO] testing: rollback
# 2024-08-30 16:47:18,017 [DEBUG] testing: 4
# 2024-08-30 16:47:18,017 [INFO] testing: add
# 2024-08-30 16:47:18,018 [DEBUG] testing: 4
# 2024-08-30 16:47:18,018 [INFO] testing: flush
# 2024-08-30 16:47:18,019 [DEBUG] testing: 5
# 2024-08-30 16:47:18,020 [INFO] testing: add
# 2024-08-30 16:47:18,020 [INFO] testing: flush
# 2024-08-30 16:47:18,022 [DEBUG] testing: 6
# 2024-08-30 16:47:18,022 [INFO] testing: add
# 2024-08-30 16:47:18,022 [INFO] testing: flush
# 2024-08-30 16:47:18,023 [ERROR] testing: (psycopg.errors.UniqueViolation) duplicate key value violates unique constraint "project_pkey"
# DETAIL:  Key (project_id)=(fb50fd1c-a303-445f-8d13-7d9fb18d413b) already exists.
# [SQL: INSERT INTO project (project_id, name, description, type, setting_motivational_quotes) VALUES (%(project_id)s::UUID, %(name)s::VARCHAR, %(description)s::VARCHAR, %(type)s, %(setting_motivational_quotes)s) RETURNING project.time_created]
# [parameters: {'project_id': UUID('fb50fd1c-a303-445f-8d13-7d9fb18d413b'), 'name': 'TA5', 'description': '', 'type': 'academic', 'setting_motivational_quotes': True}]
# (Background on this error at: https://sqlalche.me/e/20/gkpj)
# 2024-08-30 16:47:18,023 [INFO] testing: post exception
# 2024-08-30 16:47:18,024 [DEBUG] testing: 5
# 2024-08-30 16:47:18,024 [INFO] testing: delete
# 2024-08-30 16:47:18,034 [DEBUG] testing: 4