import asyncio
from collections import defaultdict

from nacsos_data.db.crud.items.academic import read_known_ids_map, read_item_ids_for_import
from nacsos_data.util import gather_async
from line_profiler_pycharm import profile
import logging
import uuid
import time
from contextlib import contextmanager
from timeit import default_timer
from datetime import timedelta

from matplotlib import pyplot as plt
import pandas as pd
import numpy as np
from nacsos_data.util.academic.importer import _check_known_identifiers
from nacsos_data.util.text import tokenise_item
from sqlalchemy import select, func, distinct, or_, and_, text, union, union_all, literal

from nacsos_data.db import get_engine_async
from nacsos_data.db.schemas.imports import Import, m2m_import_item_table
from nacsos_data.db.schemas.items.academic import AcademicItem, AcademicItemVariant
from nacsos_data.db.schemas.items.base import Item
from nacsos_data.db.schemas import AssignmentScope, Assignment, Annotation, BotAnnotationMetaData, BotAnnotation
from nacsos_data.util.academic.duplicate import str_to_title_slug

from nacsos_data.util.academic.readers.openalex import generate_items_from_openalex
from dotenv import dotenv_values

CONFIG = 'config/remote.env'
config = dotenv_values(CONFIG)
db_engine = get_engine_async(conf_file=CONFIG)

logging.basicConfig(format='%(asctime)s [%(levelname)s] %(name)s: %(message)s', level=logging.INFO)
logger = logging.getLogger('testing')
logger.setLevel(logging.DEBUG)

PROJECT_ID = '52f62f17-cefb-4152-bcca-2fcb77541e83'  # >1.3M items
IMPORT_1 = 'f5a1e329-5411-47be-aad9-0dac4c81d20c'  # ~80k
IMPORT_2 = '525af911-d83a-4e97-99a0-a420acfb3373'  # ~1.2M

ID_FIELDS = ['openalex_id', 's2_id', 'scopus_id', 'wos_id', 'pubmed_id', 'dimensions_id']


@contextmanager
def elapsed_timer(tn: str = 'Task'):
    # https://stackoverflow.com/questions/7370801/how-do-i-measure-elapsed-time-in-python
    start = default_timer()
    elapser = lambda: default_timer() - start
    yield lambda: elapser()
    end = default_timer()
    elapser = lambda: end - start
    logger.debug(f'{tn} took {timedelta(seconds=end - start)} to execute.')


async def main():
    async with db_engine.session() as session:
        logger.info('Starting')
        known_ids = {}
        for id_field in ID_FIELDS:
            logger.info(f'Fetching for {id_field}')
            with elapsed_timer(f'Fetching IDs for {id_field}'):
                known_ids[id_field] = await read_known_ids_map(session=session, project_id=PROJECT_ID, field=id_field)

        # Fetch item_ids already in the import (in case the import failed at some point or is continued mid-way)
        #imported_item_ids = set(await gather_async(read_item_ids_for_import(session=session, import_id=import_id)))


if __name__ == '__main__':
    asyncio.run(main())
