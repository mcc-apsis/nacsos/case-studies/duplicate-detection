# Module for duplicate detection

This project contains a prototype for duplicate detection in preparation for such functionality in NACSOS 2.
The `duplicate_detection` module is somewhat modular and highly configurable and can be persisted to disk.
It uses a dataclass to simulate a model for `Paper` and simulates DB access, so it should be easy to incorporate
into the architecture, once we have it.

The `scripts/profile_matcher.py` is a reasonable entry point without much overhead.
The most expensive part is in `match.py`, so all the candidate fetching etc. doesn't really
need profiling.

## Performance

- Time to query 100k candidates: 11.1512s
- Time to check candidates: 4.6687s
- Missed duplicates: at least 1k
- Most common number of candidates per paper:
    ```
    [(10, 39129), (1, 31373), (2, 11826), (3, 5031), (4, 3296), (5, 2361), (6, 1932), (7, 1546), (8, 1405), (9, 1215), (0, 886)]
    ```
- Most common number of duplicates per paper:
    ```
    [(1, 81486), (2, 15396), (3, 1116), (0, 1091), (4, 341), (10, 173), (5, 168), (6, 77), (9, 54), (7, 52), (8, 46)]
    ```
- Some issues with false-positives based on authors, e.g.
    ```
    # different authors, still considered duplicate 
    Paper(db_id=1018, title='NEA - nuclear power and climate change', year=1998, authors=['[Anonymous]'], key='WOS:000076665400005', abstract=None, num_pages=None, pages=None, doi=None, isbn=None, volume=None, number=None)
    Paper(db_id=234724, title='Nuclear power and climate change', year=1999, authors=['C. Kronick'], key='2-s2.0-0032809501', abstract=None, num_pages=None, pages=None, doi=None, isbn=None, volume=None, number=None)
  
    # same here, but this is actually fine
    Paper(db_id=101246, title='Sustainable nations: What do aggregate indexes tell us?', year=2010, authors=['J.R. Pillarisetti', 'J.C.J.M. van den Bergh'], key='2-s2.0-73649113842', abstract=None, num_pages=None, pages=None, doi=None, isbn=None, volume=None, number=None)
    Paper(db_id=1834428, title='Sustainable nations: what do aggregate indexes tell us?', year=2010, authors=[], key='BCI:BCI201000101874', abstract=None, num_pages=None, pages=None, doi=None, isbn=None, volume=None, number=None)
    ```
- Some fun samples for later evaluation:
    ```
    # very similar title, but different paper
    Paper(db_id=2209849, title='Resistance to high-temperature oxidation in B+Si implanted TiN coatings on steel', year=2003, authors=[], key='WOS:000181736800005', abstract=None, num_pages=None, pages=None, doi=None, isbn=None, volume=None, number=None)
    Paper(db_id=2257686, title='Resistance to high temperature oxidation in Si-implanted TiN coatings on steel', year=2002, authors=[], key='WOS:000183068800018', abstract=None, num_pages=None, pages=None, doi=None, isbn=None, volume=None, number=None)
    ```
- Is this a correct true-positive match?
    ```
    Paper(db_id=67059, title='Creating a climate for change: Communicating climate change and facilitating social change', year=2007, authors=['T Wainwright'], key='WOS:000250112600033', abstract=None, num_pages=None, pages=None, doi=None, isbn=None, volume=None, number=None)
    Paper(db_id=90691, title='Creating a Climate for Change: Communicating Climate Change and Facilitating Social Change', year=2009, authors=['MT Boykoff'], key='WOS:000265226200009', abstract=None, num_pages=None, pages=None, doi=None, isbn=None, volume=None, number=None)
    Paper(db_id=101190, title='Creating a Climate for Change: Communicating Climate Change and Facilitating Social Change.', year=2009, authors=['J Shanahan', 'J Shanahan'], key='WOS:000263822500008', abstract=None, num_pages=None, pages=None, doi=None, isbn=None, volume=None, number=None)
    Paper(db_id=129141, title='Creating a Climate for Change: Communicating Climate Change and Facilitating Social Change', year=2008, authors=['A Karner'], key='WOS:000260617900006', abstract=None, num_pages=None, pages=None, doi=None, isbn=None, volume=None, number=None)
    Paper(db_id=182724, title='Creating a climate for change: Communicating climate change and facilitating social change', year=2008, authors=['M Svoboda'], key='WOS:000255276000009', abstract=None, num_pages=None, pages=None, doi=None, isbn=None, volume=None, number=None)
    Paper(db_id=194196, title='Creating a climate for change: communicating climate change and facilitating social change', year=2010, authors=['I Bailey'], key='WOS:000273884000016', abstract=None, num_pages=None, pages=None, doi=None, isbn=None, volume=None, number=None)
    Paper(db_id=263879, title='Creating a climate for change. Communicating climate change and facilitating social change', year=2008, authors=['I Lorenzoni'], key='WOS:000258082100009', abstract=None, num_pages=None, pages=None, doi=None, isbn=None, volume=None, number=None)
    ```
- False Positive
    ```
    # query for 101138
    Paper(db_id=101138, title='A Review of Darkening Peaks: Glacier Retreat, Science, and Society', year=2009, authors=['J Janke'], key='WOS:000265301100013', abstract=None, num_pages=None, pages=None, doi=None, isbn=None, volume=None, number=None)
    Paper(db_id=545938, title='Darkening Peaks: Glacier Retreat, Science and Society', year=2009, authors=['SC Moser'], key='WOS:000270157700006', abstract=None, num_pages=None, pages=None, doi=None, isbn=None, volume=None, number=None)
    Paper(db_id=510381, title='Darkening peaks: Glacier retreat, science and society', year=2008, authors=['M. Price'], key='2-s2.0-84893680180', abstract=None, num_pages=None, pages=None, doi=None, isbn=None, volume=None, number=None)
    ```