import psycopg
import os
import json
from tqdm import tqdm
from psycopg.rows import  dict_row
with open('data/NACSOS/dump.json', 'w') as out:
    with psycopg.connect(f"postgresql://{os.environ['DB_USER']}:{os.environ['DB_PASS']}@{os.environ['DB_IP']}/"
                         f"{os.environ['DB_NAME']}") as conn:
        with conn.cursor(row_factory=dict_row) as cur:
            cur.execute('SELECT COUNT(1) as cnt FROM scoping_doc')
            total = cur.fetchone()
            step_size = 10000
            for offset in tqdm(range(0, total['cnt'], step_size), desc='Offset'):
                cur.execute('SELECT id as db_id, title, "PY" as year, authors, "UT_id" as key, content as abstract '
                            'FROM scoping_doc '
                            'ORDER BY id '
                            f'LIMIT {step_size} OFFSET {offset}')
                for record in tqdm(cur, desc='Write'):
                    out.write(json.dumps(record) + '\n')



