from duplicate_detection.index import Paper
from duplicate_detection.match import PaperMatcher
import json
import re
from line_profiler_pycharm import profile


def parse_authors(s: str) -> list[str]:
    if s is None or len(s) < 2:
        return []

    if re.match(r'\b([a-zA-Z.,])\b, ', s):
        # A, t, k, i, n, s, ,,  , R, i, c, h, a, r, d,  , S, .
        s = re.sub(r'\b([a-zA-Z.,])\b, ', r'\1', s)
        # Atkins,,  , Richard , S.
        s = s.replace(', ', '')
        # 'Atkins, Richard S.'

    if ',' not in s:
        return [s]
    split = s.split(',')
    max_range = len(split)
    max_range = max_range if max_range % 2 == 0 else max_range - 1
    return [f"{split[i + 1].strip()} {split[i].strip()}" for i in range(0, max_range, 2)]


def obj2paper(obj: dict, i: int) -> Paper:
    # id as db_id, title, "PY" as year, authors, "UT_id" as key, content as abstract '
    return Paper(db_id=obj['db_id'],
                 key=obj['key'],
                 title=obj['title'],
                 year=obj['year'],
                 authors=parse_authors(obj['authors']))


with open('data/NACSOS/dump.json') as f:
    papers = []
    for i, line in enumerate(f):
        if i >= 10000:
            break
        papers.append(obj2paper(json.loads(line), i))

matcher = PaperMatcher()
print(len(papers))

# with cProfile.Profile() as pr:

matcher.find_duplicates(papers[10], papers)

# s = io.StringIO()
# sortby = SortKey.CUMULATIVE
# ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
# ps.print_stats()
# print(s.getvalue())
