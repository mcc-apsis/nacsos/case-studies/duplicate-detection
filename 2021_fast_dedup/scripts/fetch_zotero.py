import json
import requests

LIMIT = 100

with open('data/zotero/dump.jsonl', 'w') as fout:
    for i in range(20000):
        print(i)
        req = requests.get(f'https://api.zotero.org/groups/2282974/items?start={i * LIMIT}&limit={LIMIT}')
        resp = req.json()
        if len(resp) == 0:
            break
        for it in resp:
            del it['links']
            del it['library']
            fout.write(json.dumps(it)+'\n')
