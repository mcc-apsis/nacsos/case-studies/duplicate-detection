import json
import os.path
import re
import time
import pandas as pd
from tqdm import tqdm
from collections import Counter
from multiprocessing import Pool

from duplicate_detection.index import Paper, PaperIndex
from duplicate_detection.match import PaperMatcher
from duplicate_detection import DeduplicationIndex


def load_zotero(filename: str, start_id: int = 0) -> list[Paper]:
    def parse_authors(obj: list[dict]) -> list[str]:
        # [{"creatorType": "author", "firstName": "George A.", "lastName": "Olah"}, ...]
        if obj is None:
            return []
        return [
            f"{a.get('firstName', '')} {a.get('lastName', '')}"
            for a in obj
            if a['creatorType'] == 'author'
        ]

    def parse_date(s: str) -> int:
        if s is None:
            return -1
        year = re.findall(r'\d{4}', s)
        if len(year) < 1:
            return -1
        return int(year[0])

    def zotero_obj_to_paper(obj: dict, db_id) -> Paper:
        return Paper(db_id=db_id,
                     key=obj['data'].get('key'),
                     title=obj['data'].get('title'),
                     year=parse_date(obj['data'].get('date')),
                     authors=parse_authors(obj['data'].get('creators'))
                     )

    with open(filename) as f:
        return [zotero_obj_to_paper(json.loads(line), i) for i, line in enumerate(f, start=start_id)]


def load_nacsos(filename: str, limit: int = None) -> tuple[dict[int, int], list[Paper]]:
    def parse_authors(s: str) -> list[str]:
        # Venter, CJ, Joubert, JW
        # Zimmermann, L., Weber, J., Straub, H., Kolokotronis, V.

        if s is None or len(s) < 2:
            return []

        if re.match(r'\b([a-zA-Z.,])\b, ', s):
            # A, t, k, i, n, s, ,,  , R, i, c, h, a, r, d,  , S, .
            s = re.sub(r'\b([a-zA-Z.,])\b, ', r'\1', s)
            # Atkins,,  , Richard , S.
            s = s.replace(', ', '')
            # 'Atkins, Richard S.'

        if ',' not in s:
            return [s]
        split = s.split(',')
        max_range = len(split)
        max_range = max_range if max_range % 2 == 0 else max_range - 1
        return [f"{split[i + 1].strip()} {split[i].strip()}" for i in range(0, max_range, 2)]

    def obj2paper(obj: dict, i: int) -> Paper:
        # id as db_id, title, "PY" as year, authors, "UT_id" as key, content as abstract '
        paper_index[obj['db_id']] = i
        return Paper(db_id=obj['db_id'],
                     key=obj['key'],
                     title=obj['title'],
                     year=obj['year'],
                     authors=parse_authors(obj['authors']))

    with open(filename) as f:
        paper_index = {}
        if limit is None:
            papers = [obj2paper(json.loads(line), i) for i, line in tqdm(enumerate(f))]
            return paper_index, papers

        papers = []
        for i, line in tqdm(enumerate(f)):
            if i >= limit:
                break
            papers.append(obj2paper(json.loads(line), i))

        return paper_index, papers


def load_nets(filename: str) -> list[Paper]:
    nets_df = pd.read_excel(filename,
                            sheet_name=0,
                            header=0,
                            index_col=0)
    nets_df.head()

    nets = [{
        'title': row[1]['Title'],
        'year': None if np.isnan(row[1]['Publication_Year']) else int(row[1]['Publication_Year']),
        'authors': [] if type(row[1]['Authors']) != str else [s.strip() for s in row[1]['Authors'].split(';')],
        'in_spreadsheet': True if row[1]['In_Spreadsheet'] == 1 else False,
        'relevant': True if row[1]['Relevant'] == 1 else False,
        'technologies': [] if type(row[1]['Technologies']) != str else [s.strip() for s in
                                                                        row[1]['Technologies'].split(';')],
    }
        for row in nets_df.iterrows()]


def get_papers_by_ids(ids: list[int], papers: list[Paper], paper_index: dict[int, int] = None):
    if paper_index is not None:
        return [papers[paper_index[db_id]] for db_id in ids if db_id in paper_index]
    return list(filter(lambda x: x.db_id in ids, papers))


def print_results():
    for q, c, d in zip(query_papers, candidate_papers, duplicates):
        print('>> Query:', q)
        print('   > Candidates:')
        for ci in c:
            print('     -', ci)
        print('   ==> Duplicate(s):', ', '.join([str(di.db_id) for di in d[1]]))


if __name__ == '__main__':
    start = time.time()

    tic = time.time()
    nacsos_index, nacsos_papers = load_nacsos('data/NACSOS/dump.json', limit=None)
    print(f'Num papers: {len(nacsos_papers)}')
    print(f'Loading time: {time.time() - tic:.4f}s (total elapsed: {time.time() - start:.4f}s)')

    tic = time.time()
    zip_file = 'data/stored_index_2.zip'
    if os.path.isfile(zip_file):
        print('Loading stored...')
        dedup = DeduplicationIndex.load(zip_file)
    else:
        print('Initialising...')
        matcher = PaperMatcher()
        index = PaperIndex()
        print('Extracting titles and db_ids...')
        pool = Pool()
        titles = pool.map(lambda p: p.title, nacsos_papers)
        db_ids = pool.map(lambda p: p.db_id, nacsos_papers)
        pool.close()
        print(f'Init time so far: {time.time() - tic:.4f}s  (total elapsed: {time.time() - start:.4f}s)')

        print('Initialising index...')
        index.init_index(titles=titles, db_ids=db_ids)
        print(f'Init time so far: {time.time() - tic:.4f}s  (total elapsed: {time.time() - start:.4f}s)')

        print('Wrapping up and storing...')
        dedup = DeduplicationIndex(paper_index=index, paper_matcher=matcher)
        dedup.store(zip_file)

    print(f'Index init time: {time.time() - tic:.4f}s  (total elapsed: {time.time() - start:.4f}s)')

    dedup.set_num_candidates(10)
    dedup.set_min_similarity(15.)

    tic = time.time()
    query_papers = nacsos_papers[:100000]
    candidates = dedup.get_candidates(query_papers)
    print(f'Candidate query time: {time.time() - tic:.4f}s  '
          f'(total elapsed: {time.time() - start:.4f}s)')

    print('Most common number of candidates per paper:')
    print(Counter([len(ids) for ids in candidates]).most_common(20))

    print('Doing fake DB lookup to get paper data...')
    candidate_papers = [get_papers_by_ids(ids, nacsos_papers, paper_index=nacsos_index) for ids in candidates]

    print('Checking candidates...')
    tic = time.time()
    duplicates = dedup.check_candidates(query_papers, candidate_papers)
    print(f'Candidate check time: {time.time() - tic:.4f}s  '
          f'(total elapsed: {time.time() - start:.4f}s)')

    print(f'Total runtime: {time.time() - start:.4f}s')

    print('Most common number of duplicates per paper:')
    print(Counter([len(dups) for _, dups in duplicates]).most_common(20))
    # print_results()
