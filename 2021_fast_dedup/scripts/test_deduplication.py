import json
import os.path
import re
import time
import pandas as pd

from duplicate_detection.index import Paper, PaperIndex
from duplicate_detection.match import PaperMatcher
from duplicate_detection import DeduplicationIndex


def load_zotero(filename: str, start_id: int = 0) -> list[Paper]:
    def parse_authors(obj: list[dict]) -> list[str]:
        # [{"creatorType": "author", "firstName": "George A.", "lastName": "Olah"}, ...]
        if obj is None:
            return []
        return [
            f"{a.get('firstName', '')} {a.get('lastName', '')}"
            for a in obj
            if a['creatorType'] == 'author'
        ]

    def parse_date(s: str) -> int:
        if s is None:
            return -1
        year = re.findall(r'\d{4}', s)
        if len(year) < 1:
            return -1
        return int(year[0])

    def zotero_obj_to_paper(obj: dict, db_id) -> Paper:
        return Paper(db_id=db_id,
                     key=obj['data'].get('key'),
                     title=obj['data'].get('title'),
                     year=parse_date(obj['data'].get('date')),
                     authors=parse_authors(obj['data'].get('creators'))
                     )

    with open(filename) as f:
        return [zotero_obj_to_paper(json.loads(line), i) for i, line in enumerate(f, start=start_id)]


def load_nacsos(filename: str) -> list[Paper]:
    def parse_authors(s: str) -> list[str]:
        # Venter, CJ, Joubert, JW
        # Zimmermann, L., Weber, J., Straub, H., Kolokotronis, V.
        split = s.split(',')
        return [f"{split[i + 1]} {split[i]}" for i in range(0, len(split), 2)]

    def obj2paper(obj: dict) -> Paper:
        # id as db_id, title, "PY" as year, authors, "UT_id" as key, content as abstract '
        return Paper(db_id=obj['db_id'],
                     key=obj['key'],
                     title=obj['title'],
                     year=obj['year'],
                     authors=parse_authors(obj['authors']))

    with open(filename) as f:
        return [obj2paper(json.loads(line)) for line in f]


def load_nets(filename: str) -> list[Paper]:
    nets_df = pd.read_excel(filename,
                            sheet_name=0,
                            header=0,
                            index_col=0)
    nets_df.head()

    nets = [{
        'title': row[1]['Title'],
        'year': None if np.isnan(row[1]['Publication_Year']) else int(row[1]['Publication_Year']),
        'authors': [] if type(row[1]['Authors']) != str else [s.strip() for s in row[1]['Authors'].split(';')],
        'in_spreadsheet': True if row[1]['In_Spreadsheet'] == 1 else False,
        'relevant': True if row[1]['Relevant'] == 1 else False,
        'technologies': [] if type(row[1]['Technologies']) != str else [s.strip() for s in
                                                                        row[1]['Technologies'].split(';')],
    }
        for row in nets_df.iterrows()]


def get_papers_by_ids(ids, papers):
    return list(filter(lambda x: x.db_id in ids, papers))


if __name__ == '__main__':
    start = time.time()

    tic = time.time()
    zotero_papers = load_zotero('data/zotero/dump.jsonl')
    print(f'Num papers before filter: {len(zotero_papers)}')
    zotero_papers = [p for p in zotero_papers if p.title is not None and len(p.title) > 2]
    print(f'Num papers after filter: {len(zotero_papers)}')
    print(f'Loading time: {time.time() - tic:.4f}s (total elapsed: {time.time() - start:.4f}s)')

    tic = time.time()
    zip_file = 'data/stored_index_2.zip'
    if os.path.isfile(zip_file):
        print('Loading stored...')
        dedup = DeduplicationIndex.load(zip_file)
    else:
        print('Initialising...')
        matcher = PaperMatcher()
        index = PaperIndex()
        index.init_index(titles=[p.title for p in zotero_papers],
                         db_ids=[p.db_id for p in zotero_papers])
        dedup = DeduplicationIndex(paper_index=index, paper_matcher=matcher)
        dedup.store(zip_file)
    print(f'Index init time: {time.time() - tic:.4f}s  (total elapsed: {time.time() - start:.4f}s)')

    tic = time.time()
    query_papers = zotero_papers[50:52]
    candidates = dedup.get_candidates(query_papers)
    print(f'Candidate query time: {time.time() - tic:.4f}s  '
          f'(total elapsed: {time.time() - start:.4f}s)')
    candidate_papers = [get_papers_by_ids(ids, zotero_papers) for ids in candidates]

    tic = time.time()
    duplicates = dedup.check_candidates(query_papers, candidate_papers)
    print(f'Candidate check time: {time.time() - tic:.4f}s  '
          f'(total elapsed: {time.time() - start:.4f}s)')

    for q, c, d in zip(query_papers, candidate_papers, duplicates):
        print('>> Query:', q)
        print('   > Candidates:')
        for ci in c:
            print('     -', ci)
        print('   ==>', d)

    print(f'Total runtime: {time.time() - start:.4f}s')
