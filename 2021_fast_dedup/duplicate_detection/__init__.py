from tqdm import tqdm
from typing import Union
from zipfile import ZipFile

from .match import PaperMatcher
from .index import Paper, PaperIndex, IndexParams, IndexInitParams, VectorizerParams


class DeduplicationIndex:
    def __init__(self, paper_index: PaperIndex = None, paper_matcher: PaperMatcher = None,
                 num_candidates: int = 10, min_similarity: float = 0.8):
        self.index: PaperIndex = paper_index
        self.matcher: PaperMatcher = paper_matcher
        self.num_candidates: int = num_candidates
        self.min_similarity: float = min_similarity

    def set_num_candidates(self, num_candidates: int):
        self.num_candidates = num_candidates

    def set_min_similarity(self, min_similarity: float):
        self.min_similarity = min_similarity

    def get_candidates(self, papers: list[Paper], include_similarities: bool = False) \
            -> list[list[Union[int, tuple[int, float]]]]:
        titles = [paper.title for paper in papers]
        if include_similarities:
            def pluck(c, d):
                return c, d
        else:
            def pluck(c, d):
                return c

        return [[pluck(candidate, dist) for candidate, dist in zip(candidates, distances) if dist < self.min_similarity]
                for candidates, distances in self.index.query(samples=titles, n_neighbours=self.num_candidates)]

    def check_candidates(self, papers: list[Paper], candidates: list[list[Paper]]) -> list[tuple[Paper, list[Paper]]]:
        return [(paper_i, self.matcher.find_duplicates(paper_i, candidates_i))
                for paper_i, candidates_i in tqdm(zip(papers, candidates))]

    @staticmethod
    def load(filename: str):
        with ZipFile(filename, mode='r') as in_zip:
            indx = PaperIndex.load(in_zip)
            matcher = PaperMatcher.load(in_zip)
            return DeduplicationIndex(paper_index=indx, paper_matcher=matcher)

    def store(self, filename: str):
        with ZipFile(filename, 'w') as out_zip:
            self.index.store(out_zip)
            self.matcher.store(out_zip)
