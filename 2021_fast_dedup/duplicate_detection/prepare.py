import re


def list2pattern(lst: list[str]) -> re.Pattern:
    return re.compile('\b(' + '|'.join(lst) + ')\b')


# performance hint: compiling regexes beforehand is much faster then in-place regexes
JUNK_WORDS = ['the', 'a', 'for', 'is', 'an', 'are', 'to']
REGEX_JUNK_WORDS = list2pattern(JUNK_WORDS)
REGEX_TOKEN_PATTERN = re.compile(r'(?u)\b\w\w+\b')
REGEX_NON_ALPHA = re.compile(r'[^a-z ]+')
REGEX_NON_ALPHA_NUM = re.compile(r'[^a-z0-9 ]+', flags=re.IGNORECASE)
REGEX_WHITESPACES = re.compile(r'\s*[,.\s]\s*')
REGEX_NUM_RANK = re.compile(r'^[0-9]+(st|nd|rd|th)$')


def norm_title(s: str) -> str:
    if s is None:
        return ''
    s = s.lower()
    s = remove_junk_words(s)
    s = REGEX_TOKEN_PATTERN.findall(s)
    s = ' '.join(s)
    s = REGEX_NON_ALPHA.sub('', s)
    return s


def strip_noise(s: str) -> str:
    """
    Remove reference 'noise' from a string
    :param s: The string to remove the noise from
    :return: The input string with all noise removed
    """
    return REGEX_NON_ALPHA_NUM.sub('', s)


def remove_junk_words(s: str, junk_words: list[str] = None) -> str:
    if junk_words is not None:
        junk_pattern = list2pattern(junk_words)
    else:
        junk_pattern = REGEX_JUNK_WORDS
    return junk_pattern.sub('', s)


def split_author(author: str) -> list[str]:
    """
    Splits an author string into its component parts
    :param  author The raw author string to split
    :return  An array composed of lastname, initial/name
    """
    parts = REGEX_WHITESPACES.split(author)
    parts = [i for i in parts if len(i) >= 1]  # Strip out blanks
    parts = [i for i in parts if not REGEX_NUM_RANK.match(i)]  # Strip out descendent numerics (e.g. '1st', '23rd')

    return parts
