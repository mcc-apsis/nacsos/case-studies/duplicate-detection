import jellyfish


def levenshtein_distance(s1: str, s2: str) -> int:
    return jellyfish.levenshtein_distance(s1, s2)


def jaro_winkler_distance(s1: str, s2: str) -> float:
    return jellyfish.jaro_winkler_similarity(s1, s2, long_tolerance=True)
