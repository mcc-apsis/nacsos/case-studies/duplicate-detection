import os

import hnswlib
import nmslib
import json
import numpy as np
from zipfile import ZipFile
from multiprocessing import Pool
from dataclasses import dataclass, asdict
from typing import Literal, Union, Optional
from sklearn.feature_extraction.text import CountVectorizer
import tempfile

from .prepare import norm_title


@dataclass
class Paper:
    db_id: int  # for now used as a fake db primary key
    title: str
    year: int
    authors: list[str]
    key: Optional[str] = None  # actual reference in the zotero or nacsos db
    abstract: Optional[str] = None
    num_pages: Optional[int] = None
    pages: Optional[tuple[int, int]] = None
    doi: Optional[str] = None
    isbn: Optional[str] = None
    volume: Optional[int] = None
    number: Optional[int] = None


@dataclass
class VectorizerParams:
    analyzer: Literal['word', 'char', 'char_wb'] = 'char_wb'
    vocabulary: dict[str, int] = None
    ngram_range: tuple[int, int] = (1, 1)
    max_features: int = None
    min_df: Union[int, float] = 1
    max_df: Union[int, float] = 1.0


@dataclass
class IndexInitParams:
    # https://github.com/nmslib/nmslib/blob/9662fef7cb25bccd4431dd4d4e0bfc3a7c4927d7/manual/methods.md
    method: Literal['hnsw', 'vp-tree', 'sw-graph'] = 'hnsw'
    data_type: nmslib.DataType = nmslib.DataType.SPARSE_VECTOR
    # https://github.com/nmslib/nmslib/blob/9662fef7cb25bccd4431dd4d4e0bfc3a7c4927d7/manual/spaces.md
    space: Literal['l1_sparse', 'l1', 'l2_sparse', 'l2'] = 'l1_sparse'

    def to_dict(self):
        obj = asdict(self)
        obj['data_type'] = obj['data_type'].name
        return obj

    @staticmethod
    def from_dict(obj):
        obj['data_type'] = {
            'DENSE_VECTOR': nmslib.DataType.DENSE_VECTOR,
            'DENSE_UINT8_VECTOR': nmslib.DataType.DENSE_UINT8_VECTOR,
            'SPARSE_VECTOR': nmslib.DataType.SPARSE_VECTOR,
            'OBJECT_AS_STRING': nmslib.DataType.OBJECT_AS_STRING
        }[obj['data_type']]
        return IndexInitParams(**obj)


@dataclass
class IndexParams:
    M: int = 30
    indexThreadQty: int = 4
    efConstruction: int = 100
    post: int = 0


class PaperIndex:
    def __init__(self, vect_params: VectorizerParams = None,
                 index_init_params: IndexInitParams = None,
                 index_params: IndexParams = None):
        if vect_params is None:
            vect_params = VectorizerParams()
        if index_init_params is None:
            index_init_params = IndexInitParams()
        if index_params is None:
            index_params = IndexParams()

        self.index_init_params = index_init_params
        self.index_params = index_params
        self.index: nmslib.dist.IntIndex = nmslib.init(**asdict(self.index_init_params))

        self.vect_params = vect_params
        self.vectorizer = CountVectorizer(**asdict(self.vect_params))

    def init_index(self, titles: list[str], db_ids: list[int]):
        print('Normalising titles...')
        pool = Pool()
        titles = pool.map(norm_title, titles)
        pool.close()

        print('Fitting vectorizer...')
        vectors = self.vectorizer.fit_transform(titles)
        self.vect_params.vocabulary = self.vectorizer.vocabulary_

        print('Adding vectors to index...')
        self.index.addDataPointBatch(data=vectors, ids=db_ids)
        print('Creating index...')
        self.index.createIndex(index_params=asdict(self.index_params), print_progress=False)

    def vectorize_titles(self, titles: list[str]) -> np.ndarray:
        s = [norm_title(title) for title in titles]
        return self.vectorizer.transform(s)

    def query(self, samples: Union[list[str], str], n_neighbours: int = 10) -> list[tuple[list[int], list[float]]]:
        # :return: A list of tuples of (list[ids], list[distances])
        vectors = self.vectorize_titles(samples)
        return self.index.knnQueryBatch(queries=vectors, k=n_neighbours)

    def add(self, samples: list[str], db_ids: list[int] = None):
        vectors = self.vectorize_titles(samples)
        self.index.addDataPointBatch(data=vectors, ids=db_ids)

    def store(self, out_zip: ZipFile):
        with out_zip.open('vectorizer_params.json', 'w') as f:
            f.write(json.dumps(asdict(self.vect_params)).encode('utf-8'))
        with out_zip.open('index_init_params.json', 'w') as f:
            obj = self.index_init_params.to_dict()
            f.write(json.dumps(obj).encode('utf-8'))
        with out_zip.open('index_params.json', 'w') as f:
            f.write(json.dumps(asdict(self.index_params)).encode('utf-8'))

        with tempfile.TemporaryDirectory() as tmp_dir:
            self.index.saveIndex(filename=f'{tmp_dir}/index', save_data=True)
            with out_zip.open('index.dat', 'w') as f_out, open(f'{tmp_dir}/index.dat', 'rb') as f_in:
                f_out.write(f_in.read())
            with out_zip.open('index', 'w') as f_out, open(f'{tmp_dir}/index', 'rb') as f_in:
                f_out.write(f_in.read())

    @staticmethod
    def load(in_zip: ZipFile):
        with in_zip.open('vectorizer_params.json', 'r') as f:
            vect_params = VectorizerParams(**json.loads(f.read().decode('utf-8')))
        with in_zip.open('index_init_params.json', 'r') as f:
            obj = json.loads(f.read().decode('utf-8'))
            index_init_params = IndexInitParams.from_dict(obj)
        with in_zip.open('index_params.json', 'r') as f:
            index_params = IndexParams(**json.loads(f.read().decode('utf-8')))

        paper_index = PaperIndex(vect_params=vect_params,
                                 index_init_params=index_init_params,
                                 index_params=index_params)

        with tempfile.TemporaryDirectory() as tmp_dir:
            with in_zip.open('index', 'r') as orig, open(f'{tmp_dir}/index', 'wb') as tmp:
                tmp.write(orig.read())
            with in_zip.open('index.dat', 'r') as orig, open(f'{tmp_dir}/index.dat', 'wb') as tmp:
                tmp.write(orig.read())

            paper_index.index.loadIndex(filename=f'{tmp_dir}/index', load_data=True)

        return paper_index
