import json
import re
from typing import Literal, Union
from multiprocessing import Pool
from functools import partial
from zipfile import ZipFile
import pylcs

from .index import Paper
from .prepare import remove_junk_words, strip_noise, split_author
from .similarity import jaro_winkler_distance, levenshtein_distance

REGEX_ALPHA_NUM = re.compile(r'[^a-z0-9]+')
REGEX_LOOKS_NUM = re.compile(r'^[0-9.\-]+$')
REGEX_LOOKS_NUM_WHITESPACE = re.compile(r'^\s*[0-9.\-]+\s*$')
REGEX_ONLY_NUM = re.compile(r'[^0-9]+')
REGEX_TOKEN_PATTERN = re.compile(r'(?u)\b\w\w+\b')
REGEX_YEAR_PATTERN = re.compile(f'\b([0-9]{4})\b')
REGEX_NUM_PATTERN = re.compile(f'\b([0-9]+)\b')

REASON = Literal['year', 'pages', 'number', 'isbn', 'doi', 'title', 'missing_title', 'exhausted', 'title+authors']


class PaperMatcher:
    def __init__(self, min_jaro_winkler_dist: float = 0.9, max_levenshtein_dist: int = 10,
                 match_isbn: bool = False, match_doi: bool = False, match_volume: bool = False,
                 match_number: bool = False, match_pages: bool = False, match_year: bool = True,
                 max_year_difference: int = 1, min_cont_sequence: float = 0.95):
        """
        A class for checking if papers are duplicates of one another.

        :param min_jaro_winkler_dist: min Jaro Winkler Distance two titles have to have to be considered duplicate
        :param max_levenshtein_dist: max Levenshtein Distance two titles are allowed to have to be considered duplicate
        :param match_isbn: whether or not to check ISBN similarity
        :param match_doi: whether or not to check doi similarity
        :param match_volume: whether or not to check volume similarity
        :param match_number: whether or not to check number similarity
        :param match_pages: whether or not to check page range similarity
        :param match_year: whether or not to check year similarity
        :param max_year_difference: how much wiggle room to allow between publishing year
        :param min_cont_sequence: if there is a long continuous substring with at least this relative length: duplicate
        """
        self.min_jaro_winkler_dist = min_jaro_winkler_dist
        self.max_levenshtein_dist = max_levenshtein_dist
        self.max_year_difference = max_year_difference
        self.min_cont_sequence = min_cont_sequence
        self.field_matches = {
            'isbn': match_isbn,
            'doi': match_doi,
            'volume': match_volume,
            'number': match_number,
            'pages': match_pages,
            'year': match_year
        }
        fields: list[REASON] = ['pages', 'volume', 'number', 'isbn']
        self.fields_to_check: list[REASON] = [f for f in fields if self.field_matches[f]]

    def store(self, zip_file: ZipFile):
        with zip_file.open('paper_matcher_params.json', 'w') as f:
            f.write(json.dumps({
                'min_jaro_winkler_dist': self.min_jaro_winkler_dist,
                'max_levenshtein_dist': self.max_levenshtein_dist,
                'max_year_difference': self.max_year_difference,
                'min_cont_sequence': self.min_cont_sequence,
                'match_isbn': self.field_matches['isbn'],
                'match_doi': self.field_matches['doi'],
                'match_volume': self.field_matches['volume'],
                'match_number': self.field_matches['number'],
                'match_pages': self.field_matches['pages'],
                'match_year': self.field_matches['year']
            }).encode('utf-8'))

    @staticmethod
    def load(zip_file: ZipFile):
        with zip_file.open('paper_matcher_params.json', 'r') as f:
            return PaperMatcher(**json.loads(f.read().decode('utf-8')))

    @staticmethod
    def _get_numeric(s: str) -> Union[bool, int]:
        if s.isnumeric():
            return int(s)

        if REGEX_LOOKS_NUM.match(s):
            return int(REGEX_ONLY_NUM.sub('', s))

        return False

    @staticmethod
    def _tokenise(s: str) -> list[str]:
        return REGEX_TOKEN_PATTERN.findall(s)

    # use this function if loads of candidates are expected
    def find_duplicates_pooled(self, paper: Paper, candidates: list[Paper]) -> list[Paper]:
        pool = Pool()
        are_duplicate_func = partial(self.are_duplicate, paper)
        duplicates = pool.map(are_duplicate_func, candidates)
        pool.close()

        return [candidates[i] for i, d in enumerate(duplicates) if d[1]]

    def find_duplicates(self, paper: Paper, candidates: list[Paper]) -> list[Paper]:
        return [candidate for candidate in candidates if self.are_duplicate(paper, candidate)[1]]

    # https://github.com/IEBH/sra-dedupe/blob/master/index.js
    def are_duplicate(self, ref1: Paper, ref2: Paper) -> tuple[REASON, bool]:
        # basic check, if titles are there
        if ref1.title is None or ref2.title is None or len(ref1.title) < 5 or len(ref2.title) < 5:
            return 'missing_title', False

        # check if DOI matches, if so: definitely a duplicate
        if self.field_matches['doi'] and ref1.doi and ref2.doi:
            return 'doi', ref1.doi == ref2.doi

        # check if there is a field (marked as important) mismatching, if so: not a duplicate
        for field in self.fields_to_check:
            if getattr(ref1, field) and getattr(ref2, field) and getattr(ref1, field) != getattr(ref2, field):
                return field, False

        # check if the years are within the predefined maximum range, if not: not a duplicate
        if self.field_matches['year'] \
                and ref1.year is not None and ref2.year is not None \
                and abs(ref1.year - ref2.year) > self.max_year_difference:
            return 'year', False

        # check if there are mentions of years in the title, if so and they mismatch: not a duplicate
        # The following is pretty much the same as the one below, remove for now to save exec time.
        # ref1_years = REGEX_YEAR_PATTERN.findall(ref1.title)
        # ref2_years = REGEX_YEAR_PATTERN.findall(ref2.title)
        # if len(ref1_years) > 0 and len(ref2_years) > 0 and set(ref1_years) != set(ref2_years):
        #     return 'year', False  #

        # check if there are mentions of numbers in the title (e.g. Part 1), if so and they mismatch: not a duplicate
        ref1_num = REGEX_NUM_PATTERN.findall(ref1.title)
        ref2_num = REGEX_NUM_PATTERN.findall(ref2.title)
        if len(ref1_num) > 0 and len(ref2_num) > 0 and set(ref1_num) != set(ref2_num):
            return 'title', False

        # performance hint: name comparison is faster, do this first!
        if compare_names(ref1.authors, ref2.authors) and \
                self.compare_titles(ref1.title, ref2.title):
            return 'title+authors', True

        return 'exhausted', False

    def compare_titles(self, t1: str, t2: str) -> bool:
        # lowercase, throw away stopwords, tokenise (implicitly gets rid of several symbols)
        title1_tokens = REGEX_TOKEN_PATTERN.findall(remove_junk_words(t1.lower()))
        title2_tokens = REGEX_TOKEN_PATTERN.findall(remove_junk_words(t2.lower()))

        title1 = ' '.join(title1_tokens)
        title2 = ' '.join(title2_tokens)

        # if titles match perfectly: is duplicate
        if title1 == title2:
            return True

        # if a significantly long continuous substring matches: is duplicate
        # performance hint: this operation is the most expensive (65% of entire function)!
        if self.min_cont_sequence <= (pylcs.lcs2(title1, title2) / min(len(title1), len(title2))):
            return True

        # if string similarities between JW and LS thresholds: is duplicate
        # performance hints: jw is cheaper to calculate, so do this first and only do levenshtein if needed
        if jaro_winkler_distance(title1, title2) >= self.min_jaro_winkler_dist and \
                levenshtein_distance(title1, title2) < self.max_levenshtein_dist:
            return True

        return False


def fuzzy_string_compare(a: str, b: str, tolerance: int = 10) -> bool:
    """
    Fuzzily compare strings a and b
    :param  a The first string to compare
    :param  b The second string to compare
    :param  tolerance The tolerance when comparing using levenshtein, defaults to 10
    :return  True if a ≈ b
    """
    if a == b:
        return True

    a = strip_noise(a).lower()[:255]
    b = strip_noise(b).lower()[:255]

    return levenshtein_distance(a, b) < tolerance


REGEX_AUTHOR_SPLIT = re.compile(r'\s*;\s*')


def split_author_string(s: str) -> list[str]:
    """
    Splits a single string of multiple authors into an array
    :param s The string to split
    :return The array of extracted authors
    """
    return re.split(REGEX_AUTHOR_SPLIT, s)


# FIXME this seems to be wrong:
# Paper(db_id=1018, title='NEA - nuclear power and climate change', year=1998, authors=['[Anonymous]'], key='WOS:000076665400005', abstract=None, num_pages=None, pages=None, doi=None, isbn=None, volume=None, number=None)
# Paper(db_id=234724, title='Nuclear power and climate change', year=1999, authors=['C. Kronick'], key='2-s2.0-0032809501', abstract=None, num_pages=None, pages=None, doi=None, isbn=None, volume=None, number=None)
# why do the authors match?

# https://raw.githubusercontent.com/hash-bang/compare-names/master/index.js
def compare_names(a: Union[str, list[str]], b: Union[str, list[str]]) -> bool:
    """
    Compare an array of authors against a second array
    :param a The first array of authors
    :param b The second array of authors
    :return True if a ≈ b
    """
    if type(a) != list:
        a = split_author_string(a)
    if type(b) != list:
        b = split_author_string(b)

    a_pos = 0
    b_pos = 0
    author_limit = min(len(a), len(b))
    failed = False

    while a_pos < author_limit and b_pos < author_limit:
        if fuzzy_string_compare(a[a_pos], b[b_pos]):  # Direct or fuzzy matching of entire strings
            a_pos += 1
            b_pos += 1
        else:
            a_auth = split_author(a[a_pos])
            b_auth = split_author(b[b_pos])
            name_limit = min(len(a_auth), len(b_auth))
            name_matches = 0
            for n in range(name_limit):
                # aAuth[n] == bAuth[n] || // Direct match
                # aAuth[n].length == 1 && bAuth[n].substr(0, 1) || // A is initial and B is full name
                # bAuth[n].length == 1 && aAuth[n].substr(0, 1) ||
                # (aAuth[n].length > 1 && bAuth[n].length > 1 && fuzzyStringCompare(aAuth[n], bAuth[n], 3))
                if a_auth[n] == b_auth[n] or \
                        (len(a_auth[n]) == 1 and b_auth[n][0] == a_auth[n]) or \
                        (len(b_auth[n]) == 1 and a_auth[n][0] == b_auth[n]) or \
                        (len(a_auth[n]) > 1 and len(b_auth[n]) > 1 and fuzzy_string_compare(a_auth[n], b_auth[n], 3)):
                    name_matches += 1
            if name_matches >= name_limit:
                a_pos += 1
                b_pos += 1
            else:
                failed = True
        break
    return not failed
